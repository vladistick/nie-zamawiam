<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Services\QueueingService;

class queueingTest extends TestCase
{
    use DatabaseTransactions;
    protected $queue_service;
    protected $vote_service;

    public function setUp()
    {
        parent::setUp();
        $this->vote_service = new \App\Services\VotingService();
        $this->queue_service = new QueueingService();
    }


    public function testUserAndRestaurantChoice()
    {
        foreach (\App\Models\UserProfile::all() as $profile) {
            $profile->status = 1;
            $profile->save();
        }
        $order = new \App\Models\OrderModels\Order();
        $order->save();

        foreach ($order->user_orders as $user_order) {

            $user_order->nie_jem = false;
            $user_order->save();
            $user_restaurant_choice = new \App\Models\OrderModels\UserRestaurantChoice();
            $user_restaurant_choice->user_order_id = $user_order->id;
            $user_restaurant_choice->restaurant_id =
                \App\Models\RestaurantModels\Restaurant::all()->random(1)->first()->id;
            $user_restaurant_choice->order_id = $order->id;
            $user_restaurant_choice->save();
        }
        $this->queue_service->chooseRestaurant();
        $this->queue_service->chooseBuyer();
        $this->assertNotEmpty(\App\Models\OrderModels\Order::first()->restaurant_id);
        $this->assertNotEmpty(\App\Models\OrderModels\Order::first()->user_id);

    }


    public function testRemoveCo()
    {
        $order = new \App\Models\OrderModels\Order();
        $order->restaurant_id = 1;
        $order->save();
        $user_order = $order->user_orders()->where('user_id',1)->first();
        $user_order->nie_jem = false;
        $user_order->save();
        $product_order = new \App\Models\OrderModels\UserOrderProduct();
        $product_order->user_order_id = $user_order->id;
        $product_order->product_id = 1;
        $product_order->save();
        $this->queue_service->finaliseVoting();
        $order = \App\Models\OrderModels\Order::orderBy('created_at','desc')->first();
        dd($order->user_orders()->with(['user_order_products.product'])->get()->toArray());
    }


}