<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Services\VoteInfoService;

class votingInfoTest extends TestCase
{
    protected $vote_service;
    protected $voting_service;
    use DatabaseTransactions;
    public function setUp()
    {
        parent::setUp();
        $this->vote_service = new voteInfoService();
        $this->voting_service = new \App\Services\VotingService();
    }

    public function testGetCurrentOrder()
    {
        foreach (\App\Models\UserProfile::all() as $profile) {
            $profile->status = 1;
            $profile->save();
        }
        $order = new \App\Models\OrderModels\Order();
        $order->restaurant_id = 1;
        $order->save();
        $this->voting_service->makeOrder(1, 1);
        $this->voting_service->makeOrder(1, 1);
        $this->voting_service->makeOrder(1, 1);
        $this->assertEquals(\App\Models\OrderModels\UserOrderProduct::all()->count(),3);
    }


}