<?php
$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->group(['middleware' => 'admin'], function () use ($router) {
        $router->group(['prefix' => 'admin/products'], function () use ($router) {
            $router->get('/restaurant/{restaurant_id}', 'AdminController@getAllProducts');
            $router->post('/{restaurant_id}', 'AdminController@createProduct');
            $router->get('/{product_id}', 'AdminController@getSpecificProduct');
            $router->put('/{product_id}', 'AdminController@updateProduct');
            $router->delete('/delete/{product_id}', 'AdminController@deleteProduct');
        });
        $router->group(['prefix' => 'admin/users'], function () use ($router) {
            $router->get('/unconfirmed', 'AdminController@getUnconfirmedUsers');
            $router->get('/confirmed', 'AdminController@getConfirmedUsers');
            $router->get('/unconfirmed/{user_id}','AdminController@changeUserStatusToConfirmed');
            $router->get('/confirmed/{user_id}','AdminController@changeUserStatusToUnconfirmed');
        });

        $router->group(['prefix' => 'admin/restaurants'], function () use ($router) {
            $router->get('/{restaurant_id}','AdminController@getRestaurant');
            $router->delete('/delete/{restaurant_id}', 'AdminController@deleteRestaurant');
            $router->post('/', 'AdminController@createRestaurant');
            $router->put('/{restaurant_id}','AdminController@updateRestaurant');
        });
    });
});