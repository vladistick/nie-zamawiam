<?php
$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->group(['middleware' => 'user'], function () use ($router) {
        $router->group(['prefix' => 'order'], function () use ($router) {
            $router->get('/step', 'CurrentOrderController@getStep');
            $router->post('/co_jemy', 'CurrentOrderController@initialiseVoting');
            $router->get('/queue', 'CurrentOrderController@getQueue');
            $router->post('/nie_zamawiam', 'CurrentOrderController@NIEZAMAWIAM');
            $router->get('/restaurants', 'CurrentOrderController@getRestaurants');
            $router->post('/vote', 'CurrentOrderController@voteOnRestaurant');
            $router->get('/products', 'CurrentOrderController@getProducts');
            $router->get('/my_order', 'CurrentOrderController@getMyOrder');
            $router->post('/make_order', 'CurrentOrderController@makeOrder');
            $router->post('/remove_order', 'CurrentOrderController@deleteOrder');
            $router->post('/comment', 'CurrentOrderController@editComment');
            $router->get('/', 'CurrentOrderController@getPreviousOrder');
        });

        $router->group(['prefix' => 'history'], function () use ($router) {
            $router->get('/index/{page}', 'OrderHistoryController@getMyOrderHistory');
            $router->get('/', 'OrderHistoryController@getMyOrderHistoryPages');
            $router->get('/{order_id}', 'OrderHistoryController@getSpecificOrder');
        });

        $router->group(['prefix' => 'restaurants'], function () use ($router) {
            $router->get('/', 'RestaurantController@getAllRestaurants');
            $router->get('/{restaurant_id}', 'RestaurantController@getSpecificRestaurant');
        });
        $router->group(['prefix' => 'user'], function () use ($router) {
            $router->post('/account_number', 'AuthController@changeAccountNumber');
            $router->get('/account_number', 'AuthController@getAccountNumber');
        });
    });
});
