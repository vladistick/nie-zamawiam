<?php


$factory->define(App\Models\RestaurantModels\Restaurant::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});
