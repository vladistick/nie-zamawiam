<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{

    public function run()
    {
        $user = new User();
        $user->password = Hash::make('password');
        $user->email = 'example@example.com';
        $user->name = 'Michał Krestow';
        $user->save();
        $user_profile = $user->user_profile;
        $user_profile->status = 0;
        $user_profile->account_number = '11222233334444555566667777';
        $user_profile->save();

        $user = new User();
        $user->password = Hash::make('password');
        $user->email = 'example1@example.com';
        $user->name = 'Patryk Korkoszyński';
        $user->save();
        $user_profile = $user->user_profile;
        $user_profile->status = 1;
        $user_profile->account_number = '11222233334444555566667777';
        $user_profile->save();

        $user = new User();
        $user->password = Hash::make('password');
        $user->email = 'example2@example.com';
        $user->name = 'Bartosz Gnat';
        $user->save();
        $user_profile = $user->user_profile;
        $user_profile->status = 1;
        $user_profile->account_number = '11222233334444555566667777';
        $user_profile->save();

    }
}