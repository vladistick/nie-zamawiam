<?php

use Illuminate\Database\Seeder;
use App\Models\RestaurantModels\Restaurant;
use App\Models\RestaurantModels\Product;
class RockAndRoll extends Seeder
{
    public function run()
    {
        $restaurant = new Restaurant();
        $restaurant->name = 'Rock and Roll';
        $restaurant->save();

        $restaurant_profile = $restaurant->restaurant_profile;
        $restaurant_profile->how_to_order = 'Przez stronę pyszne.pl';
        $restaurant_profile->website = 'https://www.pyszne.pl/rock-and-roll';
        $restaurant_profile->description = 'Burgery może nie najlepsze, ale jako takie';
        $restaurant_profile->save();

        $products = [
            [
                'name' => 'Burger 1 - Simple Plan',
                'price' => 20
            ],
            [
                'name' => 'Burger 2 - Chester',
                'price' => 21
            ],
            [
                'name' => 'Burger 3 - The Rolling Stones',
                'price' => 22
            ],
            [
                'name' => 'Burger 4 - Red Hot Chili Peppers',
                'price' => 22
            ],
            [
                'name' => 'Burger 5 - Perfect',
                'price' => 22
            ],
            [
                'name' => 'Burger 6 - King of Leon',
                'price' => 39
            ],
            [
                'name' => 'Burger 7 - Dżem',
                'price' => 23
            ],
            [
                'name' => 'Burger 8 - Guns N Roses',
                'price' => 21
            ],
            [
                'name' => 'Burger 9 - Little by Little',
                'price' => 10
            ],
            [
                'name' => 'Burger 10 - Nirvana (burger z kurczakiem)',
                'price' => 20
            ],
            [
                'name' => 'Burger 11 - The Cranberries (burger z indykiem)',
                'price' => 20
            ],
            [
                'name' => 'Burger 12 - The Pirates (burger z rybą)',
                'price' => 23
            ],
            [
                'name' => 'Burger 13 - The Animals (wegeburger)',
                'price' => 20
            ],
            [
                'name' => 'Burger 14 - Travis (wegeburger)',
                'price' => 20
            ],
            [
                'name' => 'Burger 15 - French Kiss (wegeburger)',
                'price' => 20
            ]
        ];

        foreach($products as $product)
        {
            $new_product = new Product();
            $new_product->name = $product['name'];
            $new_product->price = $product['price'];
            $new_product->restaurant_id = $restaurant->id;
            $new_product->save();

        }
    }
}