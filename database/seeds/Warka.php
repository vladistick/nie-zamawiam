<?php

use Illuminate\Database\Seeder;
use App\Models\RestaurantModels\Restaurant;
use App\Models\RestaurantModels\Product;
class Warka extends Seeder
{
    public function run()
    {
        $restaurant = new Restaurant();
        $restaurant->name = 'Warka';
        $restaurant->save();

        $restaurant_profile = $restaurant->restaurant_profile;
        $restaurant_profile->how_to_order = 'Przez stronę pyszne.pl';
        $restaurant_profile->website = 'https://www.pyszne.pl/piwiarnia-warki-legnica';
        $restaurant_profile->description = 'Pizza i makarony';
        $restaurant_profile->save();

        $products = [
            [
                'name' => 'Pizza Margarita',
                'price' => 14
            ],
            [
                'name' => 'Pizza Bawarska',
                'price' => 18
            ],
            [
                'name' => 'Pizza Morawska',
                'price' => 15
            ],
            [
                'name' => 'Pizza Sudecka',
                'price' => 17
            ],
            [
                'name' => 'Pizza Hawajska',
                'price' => 22
            ],
            [
                'name' => 'Pizza Turecka',
                'price' => 17
            ],
            [
                'name' => 'Pizza Chorwacka',
                'price' => 18
            ],
            [
                'name' => 'Makaron Spaghetti Bolońskie',
                'price' => 13
            ],
            [
                'name' => 'Makaron Spaghetti Carbonara',
                'price' => 13
            ],
            [
                'name' => 'Makaron Penne Sycylijska Wendetta',
                'price' => 15
            ],
            [
                'name' => 'Makaron Penne Wenecka Gondola',
                'price' => 17
            ],
        ];

        foreach($products as $product)
        {
            $new_product = new Product();
            $new_product->name = $product['name'];
            $new_product->price = $product['price'];
            $new_product->restaurant_id = $restaurant->id;
            $new_product->save();

        }
    }
}