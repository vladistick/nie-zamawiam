<?php

use Illuminate\Database\Seeder;
use App\Models\RestaurantModels\Restaurant;
use App\Models\RestaurantModels\Product;
class ZahirKebab extends Seeder
{
    public function run()
    {
        $restaurant = new Restaurant();
        $restaurant->name = 'Zahir Kebab';
        $restaurant->save();

        $restaurant_profile = $restaurant->restaurant_profile;
        $restaurant_profile->how_to_order = 'Przez stronę pyszne.pl';
        $restaurant_profile->website = 'https://www.pyszne.pl/zahir-kebab-kolejowa';
        $restaurant_profile->description = 'Kebab, duh';
        $restaurant_profile->save();

        $products = [
            [
                'name' => 'Rollo Kebab mały',
                'price' => 8.5
            ],
            [
                'name' => 'Rollo Kebab średni',
                'price' => 12.5
            ],
            [
                'name' => 'Rollo Amerykański mały',
                'price' => 10.5
            ],
            [
                'name' => 'Rollo Amerykański średni',
                'price' => 14.5
            ],
            [
                'name' => 'Rollo kebab z serem mały',
                'price' => 9.5
            ],
            [
                'name' => 'Rollo kebab z serem średni',
                'price' => 13.5
            ],
            [
                'name' => 'Mega Rollo',
                'price' => 16
            ],
            [
                'name' => 'Mega Rollo Amerykański',
                'price' => 18
            ],
            [
                'name' => 'Mega Rollo z serem',
                'price' => 19
            ],
            [
                'name' => 'Rollo Samo Mięso mały',
                'price' => 12.5
            ],
            [
                'name' => 'Rollo Samo Mięso średni',
                'price' => 16.5
            ],
        ];

        foreach($products as $product)
        {
            $new_product = new Product();
            $new_product->name = $product['name'];
            $new_product->price = $product['price'];
            $new_product->restaurant_id = $restaurant->id;
            $new_product->save();

        }
    }
}