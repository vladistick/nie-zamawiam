<?php

namespace App\Events;

use App\Models\OrderModels\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\InteractsWithSockets;

class UserQueueEvent extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function broadcastOn(): Channel
    {
        return new Channel('queue');
    }

    public function broadcastWith()
    {
        return[
            'body' => $this->getCurrentQueue()
        ];
    }

    private function getCurrentQueue(): array
    {
        $order = Order::orderBy('created_at','desc')->first();
        $user_orders = $order->user_orders()->where('nie_jem', false)
            ->orderBy('updated_at', 'DESC')->with(['user'])->get();
        return $user_orders->toArray();
    }
}
