<?php

namespace App\Events;

use App\Models\OrderModels\CurrentStep;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Broadcasting\InteractsWithSockets;

class ChangeStepEvent extends Event implements ShouldBroadcast
{
    use InteractsWithSockets;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function broadcastOn(): Channel
    {
        return new Channel('step');
    }

    public function broadcastWith()
    {
        return[
            'body' => $this->getCurrentStep()
        ];
    }

    protected function getCurrentStep(): array
    {

        $current_step = CurrentStep::orderBy('created_at','desc')->first();
        if($current_step == null) {
            $current_step = ['step'=>4];
            return $current_step;
        }
        return $current_step->toArray();
    }
}