<?php

namespace App\Http\Middleware;

use Closure;
use App\Interfaces\JWTInterface;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use App\Models\User;
use Firebase\JWT\ExpiredException;

class AuthMiddleware
{
    protected $jwt_service;

    public function __construct(JWTInterface $jwtService)
    {
        $this->jwt_service = $jwtService;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws GeneralException
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $this->getToken($request);
        try {
            $decoded_token = $this->jwt_service->decodeToken($token);
        } catch (ExpiredException $exception) {
            throw new GeneralException('Unauthorized', 401);
        }
        $user = User::find($decoded_token->sub);
        if ($user != null) {
            $request['auth'] = $user->user_profile->toArray();
            $refreshed_token = $this->jwt_service->encodeToken($user);
            $request['token'] = $refreshed_token;
            return $next($request);
        }
        throw new GeneralException('Unauthorized', 401);
    }

    /**
     * @param Request $request
     * @return string
     * @throws GeneralException
     */
    private function getToken(Request $request): string
    {
        $token = $request->header('Authorization');
        if ($token != null&&$token != "null") {
            return $token;
        }
        throw new GeneralException('Unauthorized', 401);
    }
}