<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\UserProfile;
use App\Exceptions\GeneralException;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws GeneralException
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->auth != null) {

                if ($request->auth['status'] == UserProfile::ADMIN) {
                    return $next($request);
                }
        }
        throw new GeneralException('Unauthorized', 401);
    }

}