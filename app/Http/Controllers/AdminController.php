<?php

namespace App\Http\Controllers;

use App\Interfaces\APIResponseInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\RestaurantInterface;
use App\Interfaces\UserAccountInterface;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $auth_service;
    protected $restaurant;
    protected $product;
    protected $user;

    public function __construct(
        Request $request,
        APIResponseInterface $response,
        ProductInterface $product,
        RestaurantInterface $restaurant,
        UserAccountInterface $user)
    {
        parent::__construct($request, $response);
        $this->product = $product;
        $this->restaurant = $restaurant;
        $this->user = $user;
    }

    public function getAllProducts(int $restaurant_id)
    {
        $products = $this->product->getAllProducts($restaurant_id);
        $this->response->setData($products);
        return $this->getResponse();
    }

    public function getUnconfirmedUsers()
    {
        $users = $this->user->getUnconfirmedUsers();
        $this->response->setData($users);
        return $this->getResponse();
    }

    public function getConfirmedUsers()
    {
        $users = $this->user->getConfirmedUsers();
        $this->response->setData($users);
        return $this->getResponse();
    }

    public function deleteRestaurant(int $restaurant_id)
    {
        $this->restaurant->delete($restaurant_id);
        return $this->getResponse();
    }

    public function changeUserStatusToUnconfirmed(int $user_id)
    {
        $this->user->changeUserStatusToUnconfirmed($user_id);
        return $this->getResponse();
    }

    public function changeUserStatusToConfirmed(int $user_id)
    {
        $this->user->changeUserStatusToConfirmed($user_id);
        return $this->getResponse();
    }

    public function deleteProduct(int $product_id)
    {
        $this->product->delete($product_id);
        return $this->getResponse();
    }

    public function getRestaurant(int $restaurant_id)
    {
        $restaurant = $this->restaurant->show($restaurant_id);
        $this->response->setData($restaurant);
        return $this->getResponse();
    }

    public function createRestaurant(Request $request)
    {
        $this->restaurant->create(
            $request->phone_number,
            $request->website,
            $request->description,
            $request->how_to_order,
            $request->name
        );
        return $this->getResponse();
    }

    public function createProduct(Request $request, int $restaurant_id)
    {
        $this->product->create($restaurant_id, $request->price, $request->name);
        return $this->getResponse();
    }

    public function getSpecificProduct(int $product_id)
    {
        $product = $this->product->getSpecificProduct($product_id);
        $this->response->setData($product);
        return $this->getResponse();
    }

    public function updateProduct(int $product_id, Request $request)
    {
        $this->product->update($product_id, $request->price, $request->name);
        return $this->getResponse();
    }

    public function updateRestaurant(int $restaurant_id, Request $request)
    {
        $this->restaurant->update(
            $restaurant_id,
            $request->phone_number,
            $request->website,
            $request->description,
            $request->how_to_order,
            $request->name);
        return $this->getResponse();

    }
}