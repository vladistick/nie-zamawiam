<?php

namespace App\Http\Controllers;

use App\Interfaces\APIResponseInterface;
use App\Interfaces\OrderHistoryInterface;
use Illuminate\Http\Request;

class OrderHistoryController extends Controller
{
    protected $order_history;

    public function __construct(
        OrderHistoryInterface $order_history,
        Request $request,
        APIResponseInterface $response)
    {
        parent::__construct($request, $response);

        $this->order_history = $order_history;

    }

    public function getSpecificOrder(int $order_id)
    {
        $order = $this->order_history->getSpecificOrder($order_id);
        $this->response->setData($order);
        return $this->getResponse();

    }

    public function getMyOrderHistory(Request $request, int $page)
    {
        $order_history = $this->order_history->getMyOrderHistory($request->auth['user_id'], $page);
        $this->response->setData($order_history);
        return $this->getResponse();
    }

    public function getMyOrderHistoryPages(Request $request)
    {
        $pages = $this->order_history->getMyOrderHistoryPages($request->auth['user_id']);
        $this->response->setData(['pages' => $pages]);
        return $this->getResponse();
    }

}


