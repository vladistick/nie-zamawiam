<?php

namespace App\Http\Controllers;


use App\Interfaces\APIResponseInterface;
use App\Interfaces\AuthInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $auth_service;

    public function __construct(AuthInterface $authService, Request $request, APIResponseInterface $response)
    {
        parent::__construct($request, $response);

        $this->auth_service = $authService;
    }

    public function login(Request $request)
    {
        $data = $this->auth_service->login($request->email, $request->password);
        $this->response->setToken($data[0]);
        $this->response->setUser($data[1]);
        return $this->getResponse();

    }

    public function register(Request $request)
    {
        $this->auth_service->register($request->email, $request->password, $request->name);
        return $this->getResponse();
    }

    public function getData(Request $request)
    {
        $id = $this->auth_service->getData($request->token);
        $this->response->setData(["id" => $id->sub]);
        return $this->getResponse();
    }

    public function getAccountNumber(Request $request)
    {
        $account_number = $this->auth_service->getAccountNumber($request->auth['user_id']);
        $this->response->setData($account_number);
        return $this->getResponse();

    }

    public function changeAccountNumber(Request $request)
    {
        $this->auth_service->changeAccountNumber($request->auth['user_id'], $request->account_number);
        return $this->getResponse();
    }

}