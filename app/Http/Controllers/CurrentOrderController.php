<?php

namespace App\Http\Controllers;

use App\Interfaces\APIResponseInterface;
use App\Interfaces\InitialiseVotingInterface;
use App\Interfaces\OrderingStepInterface;
use App\Interfaces\QueueingStepInterface;
use App\Interfaces\StepInfoInterface;
use Illuminate\Http\Request;

class CurrentOrderController extends Controller
{
    protected $step_info;
    protected $queueing_step;
    protected $initialise_voting;
    protected $ordering_step;

    public function __construct(
        InitialiseVotingInterface $initialise_voting,
        OrderingStepInterface $ordering_step,
        QueueingStepInterface $queueing_step,
        StepInfoInterface $step_info,
        Request $request,
        APIResponseInterface $response)
    {
        parent::__construct($request, $response);
        $this->step_info = $step_info;
        $this->queueing_step = $queueing_step;
        $this->initialise_voting = $initialise_voting;
        $this->ordering_step = $ordering_step;
    }


    public function initialiseVoting(Request $request)
    {
        $this->initialise_voting->initialiseVoting($request->co_jemy);
        return $this->getResponse();
    }


    public function NIEZAMAWIAM(Request $request)
    {
        $this->queueing_step->NIEZAMAWIAM($request->nie_zamawiam, $request->auth['user_id']);
        return $this->getResponse();
    }

    public function getQueue(Request $request)
    {
        $queue = $this->queueing_step->getQueue($request->auth['user_id']);
        $this->response->setData($queue);
        return $this->getResponse();
    }

    public function getRestaurants(Request $request)
    {
        $restaurants = $this->queueing_step->getRestaurants($request->auth['user_id']);
        $this->response->setData($restaurants);
        return $this->getResponse();
    }

    public function voteOnRestaurant(Request $request)
    {
        $this->queueing_step->voteOnRestaurant($request->restaurant_id, $request->auth['user_id']);
        return $this->getResponse();
    }


    public function getProducts()
    {
        $products = $this->ordering_step->getProducts();
        $this->response->setData($products);
        return $this->getResponse();
    }

    public function getMyOrder(Request $request)
    {
        $order = $this->ordering_step->getMyOrder($request->auth['user_id']);
        $this->response->setData($order);
        return $this->getResponse();
    }

    public function editComment(Request $request)
    {
        $this->ordering_step->comment($request->comment, $request->auth['user_id']);
        return $this->getResponse();
    }

    public function makeOrder(Request $request)
    {
        $this->ordering_step->makeOrder($request->product_id, $request->auth['user_id']);
        return $this->getResponse();
    }

    public function deleteOrder(Request $request)
    {
        $this->ordering_step->deleteOrder($request->order_id, $request->auth['user_id']);
        return $this->getResponse();
    }


    public function getStep()
    {
        $step = $this->step_info->getStep();
        $this->response->setData($step);
        return $this->getResponse();
    }

    public function getPreviousOrder()
    {
        $order = $this->step_info->getPreviousOrder();
        $this->response->setData($order);
        return $this->getResponse();
    }
}