<?php

namespace App\Http\Controllers;

use App\Interfaces\APIResponseInterface;
use App\Interfaces\Repository;
use App\Interfaces\RestaurantInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    protected $restaurant;

    public function __construct(RestaurantInterface $restaurant, Request $request, APIResponseInterface $response)
    {
        parent::__construct($request, $response);
        $this->restaurant = $restaurant;
    }

    public function getAllRestaurants()
    {
        $restaurants = $this->restaurant->getAllRestaurants();
        $this->response->setData($restaurants);
        return $this->getResponse();
    }

    public function getSpecificRestaurant($restaurant_id): JsonResponse
    {
        $restaurant = $this->restaurant->getSpecificRestaurant($restaurant_id);
        $this->response->setData($restaurant);
        return $this->getResponse();
    }


}