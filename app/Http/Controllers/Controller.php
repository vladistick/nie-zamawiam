<?php

namespace App\Http\Controllers;

use App\Interfaces\APIResponseInterface;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;


abstract class Controller extends BaseController {

    protected $response;
    protected $request;

    public function __construct(Request $request, APIResponseInterface $response) {
    $this->request = $request;
    $this->response = $response;
        if(!empty($request->auth)) {
            $this->response->setUser($request->auth);
        }

        if(!empty($request->token)) {
            $this->response->setToken($request->token);
        }
    }

    protected function getResponse(int $status = JSONResponse::HTTP_OK): JSONResponse {
        return response()->json($this->response->get(), $status);
    }
}