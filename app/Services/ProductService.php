<?php

namespace App\Services;

use App\Interfaces\ProductInterface;
use App\Models\RestaurantModels\Product;
use App\Models\RestaurantModels\Restaurant;

class ProductService implements ProductInterface
{

    public function getAllProducts(int $restaurant_id): array
    {
        return Restaurant::find($restaurant_id)->products()->get()->toArray();
    }

    public function create(int $restaurant_id, float $price, string $name): void
    {
        if (Restaurant::find($restaurant_id) != null) {
            $product = new Product();
            $product->price = $price;
            $product->name = $name;
            $product->restaurant_id = $restaurant_id;
            $product->save();
        }
    }

    public function getSpecificProduct(int $product_id): array
    {
        $product = Product::find($product_id);
        if ($product == null) {
            return [];
        } else return $product->toArray();
    }

    public function update(int $product_id, float $price, string $name): void
    {
        $product = Product::find($product_id);
        if ($product != null) {
            $product->price = $price;
            $product->name = $name;
            $product->save();
        }
    }

    public function delete(int $product_id): void
    {
        $product = Product::find($product_id);
        if ($product != null) {
            $product->delete();
        }
    }
}