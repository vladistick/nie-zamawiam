<?php

namespace App\Services;

use App\Interfaces\UserAccountInterface;
use App\Models\User;
use App\Models\UserProfile;

class UserAccountService implements UserAccountInterface
{

    public function getUnconfirmedUsers(): array
    {
        $users = UserProfile::where('status', UserProfile::UNCONFIRMED_ACCOUNT)->with(['user'])->get();
        if ($users == null) {
            return [];
        } else return $users->toArray();
    }

    public function getConfirmedUsers(): array
    {
        $users = UserProfile::where('status', UserProfile::CONFIRMED_ACCOUNT)->with(['user'])->get();
        if ($users == null) {
            return [];
        } else return $users->toArray();
    }

    public function changeUserStatusToUnconfirmed(int $user_id): void
    {
       $user = User::find($user_id)->user_profile;
       $user->status = UserProfile::UNCONFIRMED_ACCOUNT;
       $user->save();
    }

    public function changeUserStatusToConfirmed(int $user_id): void
    {
        $user = User::find($user_id)->user_profile;
        $user->status = UserProfile::CONFIRMED_ACCOUNT;
        $user->save();
    }
}