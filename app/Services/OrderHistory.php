<?php

namespace App\Services;

use App\Interfaces\OrderHistoryInterface;
use App\Models\OrderModels\Order;
use App\Models\OrderModels\UserOrder;

class OrderHistory implements OrderHistoryInterface
{

    const PER_PAGE = 20;

    public function getSpecificOrder(int $order_id): array
    {
        $order = Order::where('id', $order_id)->with([
            'user_orders.user_order_products.product',
            'user.user_profile',
            'user_orders.user',
            'user_orders.comment',
            'restaurant.restaurant_profile'
        ])->first();
        if ($order == null) {
            return [];
        } else return $this->calculateTotalCost($order->toArray());
    }

    public function getMyOrderHistory(int $user_id, int $page): array
    {
        $user_orders = UserOrder::where('user_id', $user_id)->with(['order.restaurant', 'order.user'])
            ->limit(self::PER_PAGE)->offset(self::PER_PAGE * $page)->get()->toArray();
        return $user_orders;
    }

    public function getMyOrderHistoryPages(int $user_id): int
    {
        $user_orders = UserOrder::where('user_id', $user_id)->get()->count();
        return ceil($user_orders / self::PER_PAGE);
    }

    private function calculateTotalCost(array $order): array
    {
        $total_cost = 0;
        foreach ($order['user_orders'] as $user_order) {
            foreach ($user_order['user_order_products'] as $product) {
                $total_cost += $product['product']['price'];
            }
        }
        $order['total_cost'] = $total_cost;
        return $order;
    }

}