<?php

namespace App\Services;

use App\Interfaces\RestaurantInterface;
use App\Models\RestaurantModels\Restaurant;

class RestaurantService implements RestaurantInterface
{

    public function getAllRestaurants(): array
    {
        return Restaurant::all()->toArray();
    }

    public function getSpecificRestaurant(int $restaurant_id): array
    {
        $restaurant = Restaurant::where('id', $restaurant_id)->with(['products', 'restaurant_profile'])->first();
        if ($restaurant == null) {
            return [];
        } else return $restaurant->toArray();
    }

    public function show(int $restaurant_id): array
    {
        $restaurant = Restaurant::find($restaurant_id);
        if ($restaurant == null) {
            return [];
        } else return $restaurant->load('restaurant_profile')->toArray();
    }


    public function delete(int $restaurant_id): void
    {
        $restaurant = Restaurant::find($restaurant_id);
        if ($restaurant != null) {
            $restaurant->delete();
        }
    }

    public function create(
        string $phone_number,
        string $website,
        string $description,
        string $how_to_order,
        string $name): void
    {
        $restaurant = new Restaurant();
        $restaurant->name = $name;
        $restaurant->save();

        $restaurant_profile = $restaurant->restaurant_profile;
        $restaurant_profile->description = $description;
        $restaurant_profile->website = $website;
        $restaurant_profile->phone_number = $phone_number;
        $restaurant_profile->how_to_order = $how_to_order;
        $restaurant_profile->save();
    }

    public function update(
        int $restaurant_id,
        string $phone_number,
        string $website,
        string $description,
        string $how_to_order,
        string $name): void
    {
        $restaurant = Restaurant::find($restaurant_id);
        if ($restaurant != null) {
            $restaurant->name = $name;
            $restaurant->save();
            $restaurant_profile = $restaurant->restaurant_profile;
            $restaurant_profile->description = $description;
            $restaurant_profile->website = $website;
            $restaurant_profile->how_to_order = $how_to_order;
            $restaurant_profile->phone_number = $phone_number;
            $restaurant_profile->save();
        }
    }
}