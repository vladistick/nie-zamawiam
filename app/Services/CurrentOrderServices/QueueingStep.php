<?php

namespace App\Services\CurrentOrderServices;

use App\Events\UserQueueEvent;
use App\Interfaces\QueueingStepInterface;
use App\Models\OrderModels\Order;
use App\Models\OrderModels\UserRestaurantChoice;
use App\Models\RestaurantModels\Restaurant;

class QueueingStep implements QueueingStepInterface
{
    const NIE_ZAMAWIAM = [
        'nie zamawiam', 'Nie zamawiam', 'Nie zamawiam!', 'nie zamawiam!', 'NIE ZAMAWIAM',
        'NIE ZAMAWIAM!', 'Nie zamawiam.', 'NIE ZAMAWIAM.'
    ];

    /**
     * @param string $nie_zamawiam
     * @param int $user_id
     */
    public function NIEZAMAWIAM(string $nie_zamawiam, int $user_id): void
    {
        $correctly_spelled = false;
        foreach (self::NIE_ZAMAWIAM as $value) {
            if ($value == $nie_zamawiam) {
                $correctly_spelled = true;
                break;
            }
        }
        if ($correctly_spelled == true) {
            $order = Order::orderBy('created_at', 'desc')->first();
            $user_order = $order->user_orders()->where("user_id", $user_id)->first();
            $user_order->nie_jem = false;
            $user_order->save();
            event(new UserQueueEvent());
        }
    }

    public function getQueue(int $user_id): array
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $user_orders = $order->user_orders()->where('nie_jem', false)
            ->orderBy('updated_at', 'DESC')->with(['user'])->get();
        if ($user_orders != null) {
            if ($order->user_orders()->where('nie_jem', false)->where('user_id', $user_id)->first() != null) {
                return [$user_orders->toArray(), true];
            } else {
                return [$user_orders->toArray(), false];
            }
        } else return [null, false];
    }

    /**
     * @param int $restaurant_id
     * @param int $user_id
     */
    public function voteOnRestaurant(int $restaurant_id, int $user_id): void
    {

        $order = Order::orderBy('created_at', 'desc')->first();
        $user_order = $order->user_orders()->where('user_id', $user_id)->first();
        $previous_vote = UserRestaurantChoice::where('user_order_id', $user_order->id)->
        where('restaurant_id', $restaurant_id)->where('order_id', $order->id)->first();
        if ($previous_vote == null) {
            $restaurant_vote = new UserRestaurantChoice();
            $restaurant_vote->order_id = $order->id;
            $restaurant_vote->user_order_id = $user_order->id;
            $restaurant_vote->restaurant_id = $restaurant_id;
            $restaurant_vote->save();
        } else {
            $previous_vote->delete();
        }
    }

    /**
     * @param int $user_id
     * @return array
     */
    public function getRestaurants(int $user_id): array
    {
        $arrayed_restaurants = [];
        $user_order = Order::orderBy('created_at', 'desc')->first()
            ->user_orders()->where('user_id', $user_id)->first();
        $restaurant_choices = $user_order->user_restaurant_choices()->get();
        $restaurants = Restaurant::all();
        foreach ($restaurants as $restaurant) {
            $upvoted = false;
            foreach ($restaurant_choices as $choice) {
                if ($choice->restaurant_id == $restaurant->id) {
                    $upvoted = true;
                    break;
                }
            }
            if ($upvoted) {
                array_push($arrayed_restaurants, [$restaurant->toArray(), true]);
            } else {
                array_push($arrayed_restaurants, [$restaurant->toArray(), false]);
            }
        }
        return $arrayed_restaurants;
    }
}