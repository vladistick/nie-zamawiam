<?php

namespace App\Services\CurrentOrderServices;

use App\Models\OrderModels\CurrentStep;
use App\Exceptions\GeneralException;
use App\Interfaces\StepInfoInterface;
use App\Models\OrderModels\Order;

class StepInfo implements StepInfoInterface
{
    /**
     * @return array
     * @throws GeneralException
     */
    public function getStep(): array
    {
        $current_step = CurrentStep::orderBy('created_at', 'desc')->first();
        if ($current_step == null) {
            throw new GeneralException("Brak zamówień", 418);
        }
        return $current_step->toArray();
    }

    /**
     * @return array
     */
    public function getPreviousOrder(): array
    {
        $order = Order::orderBy('created_at','desc')->with([
            'user_orders.user_order_products.product',
            'user.user_profile',
            'user_orders.user',
            'user_orders.comment',
            'restaurant.restaurant_profile'
        ])->first();
        return $this->calculateTotalCost($order->toArray());
    }

    private function calculateTotalCost(array $order): array
    {
        $total_cost = 0;
        foreach($order['user_orders'] as $user_order)
        {
            foreach($user_order['user_order_products'] as $product)
            {
                $total_cost += $product['product']['price'];
            }
        }
        $order['total_cost'] = $total_cost;
        return $order;
    }
}