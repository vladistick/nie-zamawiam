<?php

namespace App\Services\CurrentOrderServices;

use App\Interfaces\OrderingStepInterface;
use App\Models\OrderModels\Order;
use App\Models\OrderModels\UserOrder;
use App\Models\OrderModels\UserOrderProduct;
use App\Models\RestaurantModels\Product;

class OrderingStep implements OrderingStepInterface
{
    /**
     * @return array
     */
    public function getProducts(): array
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $products = $order->restaurant()->with(['restaurant_profile', 'products'])->get();
        return ($products->toArray())[0];
    }

    /**
     * @param int $user_id
     * @return array
     */
    public function getMyOrder(int $user_id): array
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $user_order = $order->user_orders()->where('user_id', $user_id)->with([
            'user_order_products.product', 'comment'
        ])->first();
        return $user_order->toArray();
    }

    /**
     * @param int $product_id
     * @param int $user_id
     */
    public function makeOrder(int $product_id, int $user_id): void
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $product = Product::find($product_id);

        if ($product->restaurant_id == $order->restaurant_id) {
            $user_order = UserOrder::where('user_id', $user_id)->where('order_id', $order->id)->first();
            if ($user_order->nie_jem == true) {
                $user_order->nie_jem = false;
                $user_order->save();
            }
            $product_order = new UserOrderProduct();
            $product_order->product_id = $product_id;
            $product_order->user_order_id = $user_order->id;
            $product_order->save();
        }
    }

    /**
     * @param int $id
     * @param int $user_id
     */
    public function deleteOrder(int $id, int $user_id): void
    {
        $product_order = UserOrderProduct::find($id);
        if ($product_order != null && $product_order->user_order->user_id == $user_id) {
            if ($product_order->user_order->user_id == $user_id) {
                $product_order->delete();
            }
        }
    }

    /**
     * @param string $comment
     * @param int $user_id
     */
    public function comment(string $comment, int $user_id): void
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $user_comment = $order->user_orders()->where('user_id', $user_id)->first()->comment;
        if ($user_comment != null) {
            $user_comment->comment = $comment;
            $user_comment->save();
        }
    }
}