<?php

namespace App\Services\CurrentOrderServices;

use App\Models\OrderModels\CurrentStep;
use App\Events\ChangeStepEvent;
use App\Interfaces\InitialiseVotingInterface;
use App\Jobs\OrderPick;
use App\Models\OrderModels\Order;
use Carbon\Carbon;

class InitialiseVoting implements InitialiseVotingInterface
{
    const CO_JEMY = [
        'co jemy?', 'Co jemy?', 'co jemy', 'Co jemy', 'CO JEMY?',
        'CO JEMY'
    ];

    /**
     * @param string $co_jemy
     */
    public function initialiseVoting(string $co_jemy): void
    {
        $correctly_spelled = false;
        foreach (self::CO_JEMY as $value) {
            if ($value == $co_jemy) {
                $correctly_spelled = true;
                break;
            }
        }
        if ($correctly_spelled) {
            $current_step = CurrentStep::orderBy('created_at', 'desc')->first();
            if (($current_step != null && $current_step->step == CurrentStep::CAN_CREATE_NEW_ORDER) ||
                $current_step == null) {
                $order = new Order();
                $order->save();
                $current_step = new CurrentStep();
                $current_step->order_id = $order->id;
                $current_step->step = CurrentStep::USER_QUEUING;
                $current_step->ends_in = Carbon::now()->addMinutes(env('ORDERING_DELAY'));
                $current_step->save();
                event(new ChangeStepEvent());
                $job = (new OrderPick())->delay(Carbon::now()->addMinutes(env('ORDERING_DELAY')));
                dispatch($job);
            }
        }
    }
}