<?php

namespace App\Services;

use App\Interfaces\JWTInterface;
use App\Models\User;
use Firebase\JWT\JWT;

class JWTService implements JWTInterface
{

    public function decodeToken(string $token)
    {
        $decoded_token = JWT::decode($token, env("JWT_SECRET"), [env("JWT_ALGORITHM")]);
        return $decoded_token;
    }

    public function encodeToken(User $user): string
    {
        $payload = [
            "sub" => $user->id,
            "iat" => time(),
            "exp" => time() + 100000//env("JWT_TIMEOUT")
        ];

        return JWT::encode($payload, env("JWT_SECRET"));
    }

}