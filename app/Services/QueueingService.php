<?php

namespace App\Services;

use App\Models\OrderModels\CurrentStep;
use App\Events\ChangeStepEvent;
use App\Jobs\FinalizeOrders;
use App\Models\OrderModels\Order;
use App\Models\RestaurantModels\Restaurant;
use Carbon\Carbon;

class QueueingService
{
    const STEP_FINISHED = 0;
    const STEP_QUEUEING = 1;
    const STEP_ORDERING = 2;

    public function initialiseOrderPicking(): void
    {
        $this->chooseRestaurant();
        $current_step = CurrentStep::orderBy('created_at', 'desc')->first();
        $current_step->step = self::STEP_ORDERING;
        $current_step->ends_in = Carbon::now()->addMinutes(env('FINALIZE_ORDERS_DELAY'));
        $current_step->save();
        event(new ChangeStepEvent());
        $job = (new FinalizeOrders())->delay(Carbon::now()->addMinutes(env('FINALIZE_ORDERS_DELAY')));
        dispatch($job);
    }

    public function finaliseVoting(): void
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        foreach ($order->user_orders as $order) {
            if ($order->user_order_products->count() == 0) {
                foreach($order->user_restaurant_choices as $restaurant_choice)
                {
                    $restaurant_choice->delete();
                }
                $order->comment->delete();
                $order->delete();
            }
        }

        $this->chooseBuyer();
        $current_step = CurrentStep::orderBy('created_at', 'desc')->first();
        if ($current_step->order->user_id == null) {
            $this->deleteOrder();
            event(new ChangeStepEvent());
        } else {
            $current_step->step = self::STEP_FINISHED;
            $current_step->save();
            event(new ChangeStepEvent());
        }
    }

    public function chooseRestaurant(): void
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $restaurants = Restaurant::all();
        $votes = $order->user_restaurant_choices()->get();
        if ($votes->count() == 0) {
            $order->restaurant_id = $restaurants->random(1)->first()->id;
            $order->save();
        } else {
            $vote_results = [];
            foreach ($restaurants as $restaurant) {
                $number_of_votes =
                    $restaurant->user_restaurant_choices()->where('restaurant_id', $restaurant->id)->count();
                $vote_results[$restaurant->id] = $number_of_votes;
            }
            $top_results = array_keys($vote_results, max($vote_results));
            $choose_randomly = rand(0, count($top_results) - 1);
            $order->restaurant_id = $top_results[$choose_randomly];
            $order->save();
        }
    }

    private function chooseBuyer(): void
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $last_user_order = $order->user_orders()
            ->where('nie_jem', false)->orderBy('updated_at', 'desc')->first();
        if ($last_user_order != null) {
            $order->user_id = $last_user_order->user_id;
            $order->save();
        }
    }

    private function deleteOrder(): void
    {
        $order = Order::orderBy('created_at', 'desc')->first();
        $order->current_step->delete();
        $order->delete();
    }
}