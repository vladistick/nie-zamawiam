<?php

namespace App\Services;

use App\Interfaces\APIResponseInterface;

class APIResponse implements APIResponseInterface
{

    protected $status = 200;
    protected $data = [];
    protected $token = null;
    protected $user = null;

    public function get(): array
    {
        return [
            "status" => $this->status,
            "user" => $this->user,
            "token" => $this->token,
            "data" => $this->data,
        ];
    }

    public function setToken(string $token): APIResponseInterface
    {
        $this->token = $token;
        return $this;
    }

    public function setUser(array $user): APIResponseInterface
    {
        $this->user = $user;
        return $this;
    }

    public function setStatus(string $status): APIResponseInterface
    {
        $this->status = $status;
        return $this;
    }

    public function setMessages(array $messages): APIResponseInterface
    {
        $this->messages = $messages;
        return $this;
    }

    public function setData(array $data): APIResponseInterface
    {
        $this->data = $data;
        return $this;
    }

}