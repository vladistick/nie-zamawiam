<?php

namespace App\Services;

use App\Events\TestEvent;
use App\Events\UserQueueEvent;
use App\Interfaces\AuthInterface;
use App\Interfaces\JWTInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService implements AuthInterface
{
    protected $jwt_service;

    public function __construct(JWTInterface $jwtService)
    {
        $this->jwt_service = $jwtService;
    }

    public function login($email, $password)
    {
        $user = User::where('email', $email)->first();

        if (Hash::check($password, $user->password)) {
            $token = $this->jwt_service->encodeToken($user);
        }
        $data = [$token, $user->user_profile->toArray()];
        return $data;

    }

    public function getData($token)
    {
        return $this->jwt_service->decodeToken($token);
    }

    public function register($email, $password, $name): void
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->save();
    }

    public function changeAccountNumber(int $user_id, string $account_number)
    {
        $user_profile = User::find($user_id)->user_profile;
        $user_profile->account_number = $account_number;
        $user_profile->save();
    }

    public function getAccountNumber(int $user_id)
    {
        $user_profile = User::find($user_id)->user_profile;
        return ['account_number'=>$user_profile->account_number];
    }
}