<?php

namespace App\Jobs;

use App\Services\QueueingService;
use Carbon\Carbon;

class OrderPick extends Job
{

    protected  $voting_service;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->voting_service = new QueueingService();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->voting_service->initialiseOrderPicking();
    }
}
