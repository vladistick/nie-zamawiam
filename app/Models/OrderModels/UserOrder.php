<?php

namespace App\Models\OrderModels;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property UserRestaurantChoice $user_restaurant_choices
 * @property UserOrderProduct $user_order_products
 * @property Order $order
 * @property boolean $nie_jem
 * @property User $user
 * @property Comment $comment
 */
class UserOrder extends Model
{

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function user_restaurant_choices(): HasMany
    {
        return $this->hasMany(UserRestaurantChoice::class);
    }

    public function user_order_products(): HasMany
    {
        return $this->hasMany(UserOrderProduct::class);
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function comment(): HasOne
    {
        return $this->hasOne(Comment::class);
    }
}
