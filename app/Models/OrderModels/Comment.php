<?php

namespace App\Models\OrderModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property Order $user_order
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $comment
 */
class Comment extends Model
{
    public function user_order(): BelongsTo
    {
        return $this->belongsTo(UserOrder::class, "user_order_id", "id");
    }
}
