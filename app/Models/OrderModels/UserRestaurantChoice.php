<?php

namespace App\Models\OrderModels;

use App\Models\RestaurantModels\Restaurant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property integer $restaurant_id
 * @property $order_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Order $order
 * @property Restaurant $restaurant
 */
class UserRestaurantChoice extends Model
{
    public function user_order(): BelongsTo
    {
        return $this->belongsTo(UserOrder::class, "user_order_id", "id");
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, "order_id", "id");
    }

    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class, "restaurant_id", "id")->withTrashed();
    }

}
