<?php

namespace App\Models\OrderModels;

use App\Models\RestaurantModels\Restaurant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property integer $id
 * @property integer $restaurant_id
 * @property Restaurant $restaurant
 * @property UserOrder $user_orders
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property integer $user_id
 * @property UserRestaurantChoice $user_restaurant_choices
 * @property User $user
 */
class Order extends Model
{
    public function user_orders(): HasMany
    {
        return $this->hasMany(UserOrder::class);
    }

    public function user_restaurant_choices(): HasMany
    {
        return $this->hasMany(UserRestaurantChoice::class);
    }

    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class, "restaurant_id", "id")->withTrashed();
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function restaurant_votes(): HasMany
    {
        return $this->hasMany(UserRestaurantChoice::class);
    }

    public function current_step(): HasOne
    {
        return $this->hasOne(CurrentStep::class);
    }
}
