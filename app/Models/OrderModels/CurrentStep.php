<?php

namespace App\Models\OrderModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property integer $step
 * @property integer $order_id
 * @property Order $order
 */
class CurrentStep extends Model
{
    const CAN_CREATE_NEW_ORDER = 0;
    const USER_QUEUING = 1;
    const ORDER_PICKING = 2;

    protected $hidden =[
      'created_at', 'updated_at', 'id'
    ];

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, "order_id", "id");
    }

}
