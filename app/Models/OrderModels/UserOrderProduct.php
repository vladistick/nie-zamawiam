<?php

namespace App\Models\OrderModels;

use App\Models\RestaurantModels\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer $id;
 * @property integer $user_order_id
 * @property integer $product_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property UserOrder $user_order
 * @property Product $product
 */
class UserOrderProduct extends Model
{
    public function user_order(): BelongsTo
    {
        return $this->belongsTo(UserOrder::class, "user_order_id", "id");
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, "product_id", "id")->withTrashed();
    }
}
