<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string $account_number
 * @property integer $user_id
 * @property User $user
 * @property integer $status
 */
class UserProfile extends Model
{
    const ADMIN = 0;
    const CONFIRMED_ACCOUNT = 1;
    const UNCONFIRMED_ACCOUNT = 2;

    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_number',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }
}