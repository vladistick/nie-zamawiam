<?php

namespace App\Models\RestaurantModels;

use App\Models\OrderModels\UserRestaurantChoice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property string $name
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @property Product $products
 * @property RestaurantProfile $restaurant_profile
 * @property UserRestaurantChoice $user_restaurant_choices
 * @property Carbon $deleted_at
 */
class Restaurant extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    public function restaurant_profile(): HasOne
    {
        return $this->hasOne(RestaurantProfile::class);
    }
    public function user_restaurant_choices(): HasMany
    {
        return $this->hasMany(UserRestaurantChoice::class);
    }
}
