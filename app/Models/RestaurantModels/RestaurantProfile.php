<?php

namespace App\Models\RestaurantModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $restaurant_id
 * @property string $website
 * @property string $phone_number
 * @property string $how_to_order
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @property Restaurant $restaurant
 * @property Carbon $deleted_at
 */
class RestaurantProfile extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $primaryKey = 'restaurant_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'website', 'phone_number', 'how_to_order'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'restaurant_id'
    ];

    public function restaurant(): BelongsTo
    {
        return $this->belongsTo(Restaurant::class, "restaurant_id", "id")->withTrashed();
    }
}
