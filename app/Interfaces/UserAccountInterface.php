<?php

namespace App\Interfaces;

interface UserAccountInterface
{
    public function getUnconfirmedUsers(): array;
    public function getConfirmedUsers(): array;
    public function changeUserStatusToUnconfirmed(int $user_id): void;
    public function changeUserStatusToConfirmed(int $user_id): void;

}