<?php

namespace App\Interfaces;

interface RestaurantInterface
{
    public function getAllRestaurants(): array;

    public function getSpecificRestaurant(int $restaurant_id): array;

    public function show(int $restaurant_id): array;

    public function update(
        int $restaurant_id,
        string $phone_number,
        string $website,
        string $description,
        string $how_to_order,
        string $name): void;

    public function create(
        string $phone_number,
        string $website,
        string $description,
        string $how_to_order,
        string $name): void;

    public function delete(int $restaurant_id): void;

}