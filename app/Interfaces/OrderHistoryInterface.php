<?php

namespace App\Interfaces;

interface OrderHistoryInterface
{
    public function getSpecificOrder(int $order_id): array;
    public function getMyOrderHistory(int $user_id, int $page): array;
    public function getMyOrderHistoryPages(int $user_id): int;
}