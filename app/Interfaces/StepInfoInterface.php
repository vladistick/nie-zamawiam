<?php

namespace App\Interfaces;

interface StepInfoInterface
{
    public function getStep(): array;
    public function getPreviousOrder(): array;
}