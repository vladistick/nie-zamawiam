<?php

namespace App\Interfaces;

interface QueueingStepInterface
{
    public function NIEZAMAWIAM(string $nie_zamawiam, int $user_id): void;
    public function getQueue(int $user_id): array;
    public function voteOnRestaurant(int $restaurant_id, int $user_id): void;
    public function getRestaurants(int $user_id): array;

}