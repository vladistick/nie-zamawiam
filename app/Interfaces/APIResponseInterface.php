<?php

namespace App\Interfaces;

interface APIResponseInterface
{
    public function get(): array;

    public function setToken(string $token): APIResponseInterface;

    public function setUser(array $user): APIResponseInterface;

    public function setStatus(string $status): APIResponseInterface;

    public function setMessages(array $messages): APIResponseInterface;

    public function setData(array $data): APIResponseInterface;
}