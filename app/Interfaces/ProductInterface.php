<?php

namespace App\Interfaces;

interface ProductInterface
{
    public function getAllProducts(int $restaurant_id): array;
    public function create(int $restaurant_id, float $price, string $name): void;
    public function getSpecificProduct(int $product_id): array;
    public function update(int $product_id, float $price, string $name): void;
    public function delete(int $product_id): void;
}