<?php

namespace App\Interfaces;

interface AuthInterface
{
    public function login($email, $password);
    public function register($email, $password, $name): void;
    public function getData($token);
    public function changeAccountNumber(int $user_id, string $account_number);
    public function getAccountNumber(int $user_id);
}