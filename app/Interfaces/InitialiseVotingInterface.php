<?php

namespace App\Interfaces;

interface InitialiseVotingInterface
{
    public function initialiseVoting(string $co_jemy): void;
}