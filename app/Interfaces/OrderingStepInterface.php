<?php

namespace App\Interfaces;

interface OrderingStepInterface
{
    public function getProducts(): array;
    public function getMyOrder(int $user_id): array;
    public function makeOrder(int $product_id, int $user_id): void;
    public function deleteOrder(int $id, int $user_id): void;
    public function comment(string $comment, int $user_id): void;
}