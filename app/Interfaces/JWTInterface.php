<?php

namespace App\Interfaces;

use App\Models\User;

interface JWTInterface
{
    public function decodeToken(string $token);
    public function encodeToken(User $user): string;

}