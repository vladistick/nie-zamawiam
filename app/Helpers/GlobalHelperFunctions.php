<?php

use App\Exceptions\GeneralException;

/**
 * @param string $id
 * @throws \App\Exceptions\GeneralException
 * return string $id
 */
function checkIfIdIsNumeric(string $id)
{
    if (!is_numeric($id)) {
        throw new GeneralException("Wrong id", 400);
    }
}