<?php

namespace App\Observers;

use App\Models\RestaurantModels\Restaurant;

class RestaurantObserver
{
    public function created(Restaurant $restaurant)
    {
        $restaurant->restaurant_profile()->create();
    }

}