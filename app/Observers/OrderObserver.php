<?php

namespace App\Observers;

use App\CurrentStep;
use App\Models\OrderModels\Comment;
use App\Models\OrderModels\Order;
use App\Models\OrderModels\UserOrder;
use App\Models\UserProfile;

class OrderObserver
{
    public function created(Order $order)
    {
        $user_profiles = UserProfile::where('status', '<', UserProfile::UNCONFIRMED_ACCOUNT)->get();

        foreach ($user_profiles as $user_profile) {
            $user_order = new UserOrder();
            $user_order->order_id = $order->id;
            $user_order->user_id = $user_profile->user_id;
            $user_order->save();
            $comment = new Comment();
            $comment->user_order_id = $user_order->id;
            $comment->save();
        }
    }
    public function deleting(Order $order)
    {
        foreach ($order->user_orders as $user_order) {
            $user_order->delete();
        }
    }
}