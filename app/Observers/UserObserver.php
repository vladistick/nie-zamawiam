<?php

namespace App\Observers;

use App\Models\User;

class UserObserver
{
    public function created(User $user)
    {
        $user->user_profile()->create();
    }

    public function deleting(User $user)
    {
        $user->user_profile()->delete();
    }
}