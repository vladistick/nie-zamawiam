<?php

namespace App\Providers;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\RestaurantProfileController;
use App\Interfaces\Repository;
use App\Repositories\ProductRepository;
use App\Repositories\RestaurantProfileRepository;
use App\Repositories\RestaurantRepository;
use Illuminate\Support\ServiceProvider;

class DependencyInjectionProvider extends ServiceProvider
{
    public function boot(): void
    {

    }

    protected function makeInjection(string $when, string $needs, string $give)
    {
        $this->app->when($when)->needs($needs)->give($give);
    }
}