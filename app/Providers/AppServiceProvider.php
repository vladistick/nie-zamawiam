<?php

namespace App\Providers;

use App\Models\OrderModels\Order;
use App\Models\RestaurantModels\Restaurant;
use App\Models\User;
use App\Observers\OrderObserver;
use App\Observers\RestaurantObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Interfaces\InitialiseVotingInterface','App\Services\CurrentOrderServices\InitialiseVoting');
        $this->app->bind('App\Interfaces\OrderingStepInterface','App\Services\CurrentOrderServices\OrderingStep');
        $this->app->bind('App\Interfaces\QueueingStepInterface','App\Services\CurrentOrderServices\QueueingStep');
        $this->app->bind('App\Interfaces\StepInfoInterface','App\Services\CurrentOrderServices\StepInfo');
        $this->app->bind('App\Interfaces\OrderHistoryInterface','App\Services\OrderHistory');
        $this->app->bind('App\Interfaces\JWTInterface', 'App\Services\JWTService');
        $this->app->bind('App\Interfaces\AuthInterface', 'App\Services\AuthService');
        $this->app->bind('App\Interfaces\APIResponseInterface', 'App\Services\APIResponse');
        $this->app->bind('App\Interfaces\RestaurantInterface','App\Services\RestaurantService');
        $this->app->bind('App\Interfaces\ProductInterface','App\Services\ProductService');
        $this->app->bind('App\Interfaces\UserAccountInterface','App\Services\UserAccountService');
    }

    public function boot()
    {
        User::observe(UserObserver::class);
        Restaurant::observe(RestaurantObserver::class);
        Order::observe(OrderObserver::class);
    }
}
