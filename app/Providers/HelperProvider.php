<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperProvider extends ServiceProvider
{
    public function register()
    {
       require_once __DIR__ . '/../Helpers/GlobalHelperFunctions.php';
    }
}