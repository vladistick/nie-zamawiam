<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class GeneralException extends Exception
{
    protected $message = "";
    protected $code = "";
    protected $response;

    public function __construct(string $message, string $code)
    {
        $this->message = $message;
        $this->code = $code;
    }
}
